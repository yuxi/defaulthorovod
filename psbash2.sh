#!/bin/bash


ip=$(nslookup $6 | grep "Address: " -A2 | head -n1 | awk '{print $2}')
psstr=$ip":50001"

for(( c=6; c<=$#; c++ ))
do
  ip=$(nslookup ${!c} | grep "Address: " -A2 | head -n1 | awk '{print $2}')
	echo "ip of ${!c} is $ip"
    workerstr=$workerstr$ip":50000"
	if [ $c -ne $# ]; then
		workerstr=$workerstr","
	fi
done
echo "worker_hosts is $workerstr"
echo "ps_hosts is $psstr"

python tensorflow_benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --local_parameter_device=cpu --num_gpus=$1 \
--batch_size=$2 --model=$3 --variable_update=distributed_replicated \
--job_name=$4 --ps_hosts=$psstr \
--worker_hosts=$workerstr --task_index=$5
