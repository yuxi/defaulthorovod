
import tensorflow as tf


c = []
for d in ['/gpu:0', '/gpu:1']:
  with tf.device(d):
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3])
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2])
    c.append(tf.matmul(a, b))
with tf.device('/cpu:0'):
  sum = tf.add_n(c)
# 新建session with log_device_placement并设置为True.
config = tf.ConfigProto()
config.log_device_placement = True
#config.gpu_options.visible_device_list = '[0,1]'
sess = tf.Session(config=config)
# 运行这个op.
print(sess.run(sum))
