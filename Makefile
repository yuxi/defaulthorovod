
tcphorovodlog:
	rm -rf build
	SHOULD_LOG=1 HAVE_CUDA=1 HOROVOD_WITHOUT_PYTORCH=1 python setup.py build
	SHOULD_LOG=1 HAVE_CUDA=1 HOROVOD_WITHOUT_PYTORCH=1 python setup.py install

tcphorovodnolog:
	rm -rf build
	SHOULD_LOG=0 HAVE_CUDA=1 HOROVOD_WITHOUT_PYTORCH=1 python setup.py build
	SHOULD_LOG=0 HAVE_CUDA=1 HOROVOD_WITHOUT_PYTORCH=1 python setup.py install

runmnist:
	cd examples && rm *.json && LOG_DEVICE_INFO=0 HOROVOD_TIMELINE=mnisttimeline.json mpirun -np 2 python tensorflow_mnist.py

