#!/bin/bash
#SBATCH -J fflibtest
#SBATCH -p long
#SBATCH -N 2
#SBATCH --nodelist=greina3,greina4
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:10:00
source /users/yuxihong/tfenv/bin/activate
#mpirun -hostfile hostfiles ./allreduce 10
#mpirun -hostfile hostfiles ./send_recv 10
#mpirun ./mpihello

#HOROVOD_TIMELINE=fflib2timeline.json mpirun -hostfile hostfiles python tensorflow_mnist.py
#mpirun nvprof -o 'nvvp_%p_%h.nvprof' python tensorflow_mnist.py
#mpirun python tensorflow_mnist.py
#mpirun -hostfile hostfiles python /users/yuxihong/benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=alexnet --batch_size=32 --variable_update=horovod --data_name=cifar10 --data_dir=/users/yuxihong/dataset/cifar-10-batches-py --num_batches=128 --print_training_accuracy --trace_file=tracelog --use_chrome_trace_format=True
#mpirun -hostfile hostfiles python /users/yuxihong/benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=resnet50 --batch_size=32 --variable_update=horovod --num_batches=128 --print_training_accuracy --trace_file=tracelog --use_chrome_trace_format=True
HOROVOD_TIMELINE=fflib2timeline.json mpirun python /users/yuxihong/defaulthorovod/tensorflow_benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=$2 --batch_size=10 --variable_update=$1 --num_gpus=1 --num_batches=10 --print_training_accuracy --trace_file=tracelog --use_chrome_trace_format=True
