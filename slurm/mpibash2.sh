#!/bin/bash
#SBATCH -J fflibtest
#SBATCH -p long
#SBATCH -N 2
#SBATCH --nodelist=greina8,greina17
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:10:00
source /users/yuxihong/tfenv/bin/activate
#mpirun -hostfile hostfiles ./allreduce 10
#mpirun -hostfile hostfiles ./send_recv 10
#mpirun ./mpihello

#HOROVOD_TIMELINE=fflib2timeline.json mpirun -hostfile hostfiles python tensorflow_mnist.py
#mpirun nvprof -o 'nvvp_%p_%h.nvprof' python tensorflow_mnist.py
#mpirun python tensorflow_mnist.py
#mpirun -hostfile hostfiles python /users/yuxihong/benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=alexnet --batch_size=32 --variable_update=horovod --data_name=cifar10 --data_dir=/users/yuxihong/dataset/cifar-10-batches-py --num_batches=128 --print_training_accuracy --trace_file=tracelog --use_chrome_trace_format=True
#mpirun -hostfile hostfiles python /users/yuxihong/benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=resnet50 --batch_size=32 --variable_update=horovod --num_batches=128 --print_training_accuracy --trace_file=tracelog --use_chrome_trace_format=True
if [ $1 == 'parameter_server' ]; then
python /users/yuxihong/defaulthorovod/tensorflow_benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=$2 --batch_size=32 --variable_update=parameter_server --num_batches=100 --print_training_accuracy
fi
if [ $1 == 'horovod' ]; then
HOROVOD_TIMELINE=/home/yuxihong/defaulthorovod/slurm/2nodetimeline.json mpirun python /users/yuxihong/tensorflow_benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model=$2 --batch_size=32 --variable_update=horovod --num_gpus=1 --num_batches=2 --print_training_accuracy --num_warmup_batches=2
fi

