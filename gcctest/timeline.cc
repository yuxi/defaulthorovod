// Copyright 2018 Uber Technologies, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#include <sstream>
#include <cassert>
#include <unistd.h>
#include <limits.h>
#include <sys/syscall.h>
#include <stdio.h>
#include "timeline.h"
extern "C" {
  #include <mpi.h>
}
void Timeline::tInitialize(std::string processor, std::string thread, long long int delta, long long int local_now) {
  // get delta
  this->delta = delta;
  this->local_now = local_now;
  this->last_flush_time_ = std::chrono::steady_clock::now();
  // init file
  processor += thread;
  printf("Init tfile is %s.\n",processor.c_str());
  tfile_.open(processor, std::ios::out | std::ios::trunc);
  if (tfile_.good()) {
    // Initialize the timeline with '[' character.
    tfile_ << "[" << std::endl;
    initialized_ = true;
  } else {
    std::cerr << "WARNING: Error opening the Horovod Timeline file "
              << processor << ", will not write a timeline." << std::endl;
  }
}

bool Timeline::Initialized() const { return initialized_; }


void Timeline::tStart(const std::string& t_name, char* event_name_cstr){
  const std::string event_name(event_name_cstr);
  if (!initialized_) {
    return;
  }
  std::lock_guard<std::recursive_mutex> guard(tmutex_);
  tWriteEvent(t_name, 'B', event_name);

}

void Timeline::tEnd(const std:: string& t_name){
  if (!initialized_) {
    return;
  }
  std::lock_guard<std::recursive_mutex> guard(tmutex_);
  tWriteEvent(t_name, 'E');

}

void Timeline::tWriteEvent(const std::string& t_name, const char phase,
                  const std::string& op_name,
                  const std::string& args){
  if (!tfile_.good()) {
    return;
  }

  auto now = std::chrono::steady_clock::now().time_since_epoch().count();
  auto ts_micros = now - this->local_now + delta;
  assert(phase == 'E' || phase == 'B');
  tfile_ << "{";
  tfile_ << "\"ph\": \"" << phase << "\"";
  if (phase != 'E') {
    // Not necessary for ending event.
    tfile_ << ", \"name\": \"" << op_name << "\"";
  }
  tfile_ << ", \"ts\": " << ts_micros << "";
  tfile_ << ", \"pid\": " << t_name << "";

  if (args != "") {
    tfile_ << ", \"args\": {" << args << "}";
  }
  tfile_ << "}," << std::endl;

  auto tmpnow = std::chrono::steady_clock::now();
  if (tmpnow - last_flush_time_ >= TIMELINE_FLUSH_TIME) {
    tfile_.flush();
    last_flush_time_ = tmpnow;
  }

  if (!tfile_.good()) {
    std::cerr << "WARNING: Error writing to the Horovod Timeline after it was "
                 "successfully opened, will stop writing the timeline."
              << std::endl;
  }
}
