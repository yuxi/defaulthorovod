// Copyright 2018 Uber Technologies, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef HOROVOD_TIMELINE_H
#define HOROVOD_TIMELINE_H

#include <chrono>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <mutex>


// How frequently Horovod Timeline should be flushed to disk.
#define TIMELINE_FLUSH_TIME std::chrono::seconds(1)



// Writes timeline in Chrome Tracing format. Timeline spec is from:
// https://github.com/catapult-project/catapult/tree/master/tracing
class Timeline {
public:
  void tInitialize(std::string file_name, std::string tfile_name, long long int delta, long long int now);
  bool Initialized() const;

  void tStart(const std::string& t_name, char* event_name_cstr);
  void tEnd(const std:: string& t_name);

private:
  void tWriteEvent(const std::string& t_name, const char phase,
                  const std::string& op_name = "",
                  const std::string& args = "");

  // Boolean flag indicating whether Timeline was initialized (and thus should
  // be recorded).
  bool initialized_ = false;

  // Time point when Horovod was started.
  std::chrono::steady_clock::time_point start_time_;

  // Last time stream was flushed.
  std::chrono::steady_clock::time_point last_flush_time_;

  // Timeline file.
  std::ofstream file_;

  // Another mutex for writing information about process.
  std::recursive_mutex tmutex_;

  // Process Timeline file.
  std::ofstream tfile_;

  // Mapping of tensor names to indexes. It is used to reduce size of the
  // timeline file.
  std::unordered_map<std::string, int> tensor_table_;

  // Current state of each tensor in the timeline.

  // time delta from rank 0
  long long int delta;
  // time now
  long long int local_now;
};


#endif // HOROVOD_TIMELINE_H
