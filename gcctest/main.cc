#include <string>
#include <unistd.h>
#include <limits.h>
#include <iostream>
#include <sys/syscall.h>
#include <chrono>

#include "timeline.h"

extern "C" {
	#include <mpi.h>
}

using std::cout;
using std::endl;
using namespace std::chrono;
int main(){
	MPI_Init(NULL,NULL);
	int world_size,world_rank;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	Timeline timeline;
    // MPI_Barrier(MPI_COMM_WORLD);
	// steady_clock::time_point start_time_ = steady_clock::now();
	// cout << "time since epoch count is " << start_time_.time_since_epoch().count() << endl;
	// long long int now_rank0;
	// if(world_rank == 0){
	// 	now_rank0 = start_time_.time_since_epoch().count();
	// }
	// MPI_Bcast(&now_rank0, 1,MPI_LONG_LONG, 0, MPI_COMM_WORLD);
	// cout << "rank " << world_rank << " receives " << now_rank0 << endl;
	// steady_clock::time_point local_low = steady_clock::time_point(now_rank0);
	// long long int delta_from_rank0 = now_rank0 - start_time_.time_since_epoch().count();
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
	std::string thread_name(std::to_string(syscall(SYS_gettid)));
	int a = 0;
	for(int i=0; i<100; i++){
		a += i;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	long long int local_now = steady_clock::now().time_since_epoch().count();
	cout << "rank " << world_rank << " local_now is " << local_now << endl;
	long long int rank0_now;
	if(world_rank == 0){
		rank0_now = local_now;
	}
	MPI_Bcast(&rank0_now, 1, MPI_LONG_LONG, 0, MPI_COMM_WORLD);
	long long int delta = rank0_now - local_now;
	cout << "rank " << world_rank << " delta is " << delta << endl;
	// auto ts = now - local_now;
	// long long int ts_micros =
	// 	std::chrono::duration_cast<std::chrono::microseconds>(ts).count();
	// cout << ts_micros << endl;

	timeline.tInitialize(std::string(processor_name),thread_name, delta, local_now);
	MPI_Barrier(MPI_COMM_WORLD);
	timeline.tStart(thread_name,"event");
	std::string s1 = std::string(std::to_string(getpid()));
	cout << s1 << endl;
	int i=10000;
	while(i > 0) i--;
	timeline.tEnd(thread_name);
	MPI_Finalize();
	return 0;

}
